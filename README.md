# Gothic 2 Online - Installer
Official installer for modification Gothic 2 Online.

## Prerequisites

- [git](https://git-scm.com/)
- [git lfs](https://git-lfs.com/)
- [NSIS](https://nsis.sourceforge.io/Download)

## How to Build

Right click on `installer.nsi` file and pick `Compile NSIS Script` option from context menu. 