;--------------------------------
;Includes

  !include "MUI2.nsh" ;Modern UI
  !include "LogicLib.nsh" ;Simpler Logic
  !include "WordFunc.nsh" ;VersionCompare

;--------------------------------
;Constants

  !define PROJECT_NAME "Gothic 2 Online"

  !ifndef PROJECT_VERSION
    !define PROJECT_VERSION "0.3.0.14"
  !endif

  !define PROJECT_WEBSITE "https://gothic-online.com.pl"
  !define AUTHOR "G2O Team"

  !ifndef MILESTONE_UUID
    !define MILESTONE_UUID "ffeb6081-12ea-40bd-afa9-db1ea13c0c00"
  !endif

;--------------------------------
;Compile time directives

!getdllversion "redist\VC_redist.x86.exe" REDIST_VERSION_

;--------------------------------
;Version Information

  VIProductVersion "${PROJECT_VERSION}"
  VIAddVersionKey "ProductName" "${PROJECT_NAME}"
  VIAddVersionKey "Comments" "n/a"
  VIAddVersionKey "LegalCopyright" "© ${AUTHOR}"
  VIAddVersionKey "FileDescription" "${PROJECT_NAME} installer"
  VIAddVersionKey  "FileVersion" "${PROJECT_VERSION}"
  VIAddVersionKey "OriginalFilename" "${PROJECT_VERSION}.exe"

;--------------------------------
;General

  Name "${PROJECT_NAME}"
  BrandingText "${PROJECT_NAME} - Setup"
  OutFile "${PROJECT_VERSION}.exe"
  
  Unicode True
  SetCompressor /SOLID /FINAL lzma

  InstallDir "C:\Program Files (x86)\JoWooD\Gothic II\"
  InstallDirRegKey HKCU "Software\G2O" "game_dir"

  RequestExecutionLevel admin

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING
  
  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_BITMAP "resources\header.bmp"
  !define MUI_WELCOMEFINISHPAGE_BITMAP "resources\wizard.bmp"
  
  !define MUI_FINISHPAGE_RUN
  !define MUI_FINISHPAGE_RUN_TEXT "Create desktop shortcut"
  !define MUI_FINISHPAGE_RUN_FUNCTION "CreateDesktopShortcut"
  !define MUI_FINISHPAGE_SHOWREADME ""
  !define MUI_FINISHPAGE_SHOWREADME_TEXT "Start ${PROJECT_NAME}"
  !define MUI_FINISHPAGE_SHOWREADME_FUNCTION showReadme
  
  !define MUI_FINISHPAGE_LINK "${PROJECT_NAME} website"
  !define MUI_FINISHPAGE_LINK_LOCATION ${PROJECT_WEBSITE}

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "resources\License.rtf"
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH

  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH

;--------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "English" ; The first language is the default language
  !insertmacro MUI_LANGUAGE "German"
  !insertmacro MUI_LANGUAGE "French"
  !insertmacro MUI_LANGUAGE "Italian"
  !insertmacro MUI_LANGUAGE "Spanish"
  !insertmacro MUI_LANGUAGE "Polish"
  !insertmacro MUI_LANGUAGE "Czech"
  !insertmacro MUI_LANGUAGE "Russian"
  !insertmacro MUI_LANGUAGE "Ukrainian"

;--------------------------------
;Reserve Files
  
  ;If you are using solid compression, files that are required before
  ;the actual installation should be stored first in the data block,
  ;because this will make your installer start faster.
  
  !insertmacro MUI_RESERVEFILE_LANGDLL

;--------------------------------
;Installer Sections

!define ADD_REMOVE_PROGRAMS_REG "Software\Microsoft\Windows\CurrentVersion\Uninstall\G2O"

Section "${PROJECT_NAME}" SectionG2O

  SetOutPath "$INSTDIR"

  ;Add default installation files (excluding System Pack)
  File /r /x ".gitkeep" /x "Xardas Tower.sty" /x "XT_DayStd.sgt" /x "SystemPack.vdf" /x "shw32.dll" /x "vdfs32g.dll" "files\*.*"

  ;Create Multiplayer/update_uuid text file
  FileOpen $0 "$INSTDIR\Multiplayer\update_uuid" w
  FileWrite $0 ${MILESTONE_UUID}
  FileClose $0

  ;Add g2o registry info
  WriteRegStr HKCU "SOFTWARE\G2O" "game_dir" "$INSTDIR"

  ;Add URL protocol
  WriteRegStr HKCR "g2o" "" "URL:Gothic 2 Online Protocol"
  WriteRegStr HKCR "g2o" "URL Protocol" ""
  WriteRegStr HKCR "g2o\shell\open\command" "" "$INSTDIR\Multiplayer\G2O_URLProtocol.exe $\"%1$\""
  
  ;Information for add/remove programs  
  WriteRegStr HKLM ${ADD_REMOVE_PROGRAMS_REG} "DisplayIcon" "$\"$INSTDIR\Multiplayer\Launcher\G2O_Launcher.exe$\",0"
  WriteRegStr HKLM ${ADD_REMOVE_PROGRAMS_REG} "DisplayName" "${PROJECT_NAME}"
  WriteRegStr HKLM ${ADD_REMOVE_PROGRAMS_REG} "DisplayVersion" "${PROJECT_VERSION}"
  WriteRegStr HKLM ${ADD_REMOVE_PROGRAMS_REG} "Publisher" "${AUTHOR}"
  WriteRegStr HKLM ${ADD_REMOVE_PROGRAMS_REG} "HelpLink" "${PROJECT_WEBSITE}"
  WriteRegStr HKLM ${ADD_REMOVE_PROGRAMS_REG} "UninstallString" "$\"$INSTDIR\G2O_Uninstall.exe$\""

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\G2O_Uninstall.exe"

SectionEnd

Section /o "System Pack" SectionSP

    SetOutPath "$INSTDIR"
  
    File "/oname=_Work\Data\Music\NewWorld\Xardas Tower.sty" "files\_Work\Data\Music\NewWorld\Xardas Tower.sty"
    File "/oname=_Work\Data\Music\NewWorld\XT_DayStd.sgt" "files\_Work\Data\Music\NewWorld\XT_DayStd.sgt"
    File "/oname=Data\SystemPack.vdf" "files\Data\SystemPack.vdf"
    File "/oname=System\shw32.dll" "files\System\shw32.dll"
    File "/oname=System\vdfs32g.dll" "files\System\vdfs32g.dll"

SectionEnd

Section /o "Redistributable" SectionRedist

  ${IfNot} ${Silent}
    SetOutPath "$INSTDIR"
  
    File "redist\VC_redist.x86.exe"
    ExecWait "$INSTDIR\VC_redist.x86.exe"
    Delete "$INSTDIR\VC_redist.x86.exe"
  ${EndIf}

SectionEnd

;--------------------------------
;Installer Callbacks

Function .onInit

  !insertmacro MUI_LANGDLL_DISPLAY

  ${If} ${Silent}
    Call ToggleSystemPackSection
  ${Else}
    Call ToggleRedistSection
  ${EndIf}

FunctionEnd

Function .onVerifyInstDir

  ${IfNot} ${FileExists} "$INSTDIR\System\Gothic2.exe"
    Abort
  ${EndIf}

  Call ToggleSystemPackSection

FunctionEnd

;--------------------------------
;Installer Functions

Function showReadme

  Exec "$INSTDIR/Multiplayer/Launcher/G2O_Launcher.exe"
  
FunctionEnd

Function CreateDesktopShortcut

  CreateShortCut "$DESKTOP\G2O Launcher.lnk" "$INSTDIR\Multiplayer\Launcher\G2O_Launcher.exe"

FunctionEnd

Function GetFileVersion
  ; $0: Input file path
  ; $1: Output variable for version string

  GetDLLVersion "$0" $R0 $R1

  IntOp $R2 $R0 >> 16
  IntOp $R2 $R2 & 0x0000FFFF ; Major version
  IntOp $R3 $R0 & 0x0000FFFF ; Minor version
  IntOp $R4 $R1 >> 16
  IntOp $R4 $R4 & 0x0000FFFF ; Release version
  IntOp $R5 $R1 & 0x0000FFFF ; Build version

  StrCpy $1 "$R2.$R3.$R4.$R5"
FunctionEnd

Function ToggleSystemPackSection
  ;Check Smart Heap library version
  StrCpy $0 "$INSTDIR\System\Shw32.dll"
  StrCpy $1 ""

  Call GetFileVersion

  ;Force System Pack section for vanilla game
  ${If} $1 == "6.0.0.3"
    IntOp $0 ${SF_SELECTED} | ${SF_RO}
    SectionSetFlags ${SectionSP} $0
  ${Else}
    SectionSetFlags ${SectionSP} 0
  ${EndIf}
FunctionEnd

Function ToggleRedistSection
  ;Get installed redist version from registry
  ReadRegStr $0 HKLM "SOFTWARE\Microsoft\DevDiv\VC\Servicing\14.0\RuntimeMinimum" "Version"

  ;Force redist section if redist isn't installed
  ${If} $0 S== ""
    IntOp $0 ${SF_SELECTED} | ${SF_RO}
    SectionSetFlags ${SectionRedist} $0
    Return
  ${EndIf}

  ;Get installer redist version
  StrCpy $1 "${REDIST_VERSION_1}.${REDIST_VERSION_2}.${REDIST_VERSION_3}"

  ;Compare redist version with installer redist version
  ${VersionCompare} $0 $1 $R0

  ;Force redist section if installed redist is older than installer redist
  ${If} $R0 == 2
    IntOp $0 ${SF_SELECTED} | ${SF_RO}
    SectionSetFlags ${SectionRedist} $0
  ${Else}
    SectionSetFlags ${SectionRedist} 0
  ${EndIf}
FunctionEnd

;--------------------------------
;Descriptions
  
  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${SectionG2O} "Multiplayer client"
  !insertmacro MUI_DESCRIPTION_TEXT ${SectionSP} "Gothic fixes"
	!insertmacro MUI_DESCRIPTION_TEXT ${SectionRedist} "Microsoft Visual C++ 2015-2022 (x86)"
  !insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ;Remove installation files
  RMDir /r "$INSTDIR\Multiplayer\Launcher"
  RMDir /r "$INSTDIR\Multiplayer\modules"
  RMDir /r "$INSTDIR\Multiplayer\version"
  
  Delete "$INSTDIR\Multiplayer\G2O_URLProtocol.exe"
  Delete "$INSTDIR\Multiplayer\mp.log"
  Delete "$INSTDIR\Multiplayer\update_uuid"
  Delete "$INSTDIR\System\bass.dll"
  Delete "$INSTDIR\System\crashpad_handler.exe"
  Delete "$INSTDIR\System\crashrpt.dll"
  Delete "$INSTDIR\System\dbghelp.dll"
  Delete "$INSTDIR\System\discord_game_sdk.dll"
  Delete "$INSTDIR\System\sendrpt.exe"
  Delete "$INSTDIR\G2O_Uninstall.exe"
  
  ;Remove URL Protocol
  DeleteRegKey HKCR "g2o"
  
  ;Remove information from add/remove programs
  DeleteRegKey HKLM ${ADD_REMOVE_PROGRAMS_REG}
  
  ;Remove desktop shortcut (if exists)
  Delete "$DESKTOP\G2O Launcher.lnk"

SectionEnd